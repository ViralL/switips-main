'use strict';

// ready
$(document).ready(function() {

    $(window).scroll(function(){
        if ($(window).scrollTop() >= 200) {
           $('.page-header').addClass('fixed');
        }
        else {
           $('.page-header').removeClass('fixed');
        }
    });

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).toggleClass('open').next().toggleClass('collapse');
        $('body').toggleClass('fixed');
    });
    $('.cards__item-link--js').click(function () {
        $(this).parent().parent().next().slideToggle();
        return false;
    });
    // adaptive menu

    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
          .addClass('active').siblings().removeClass('active')
          .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
      });

    $('#rest-id .control').change(function() {
        validateForm('#rest-id');
    });
    $('#regin-id .control').change(function() {
        validateForm('#regin-id');
    });
    $('#reginl-id .control').change(function() {
        validateForm('#reginl-id');
    });
    function validateForm(form) {
      var isValid = true;
      $(form + ' .btn').prop('disabled', true);
      $(form + ' .control').each(function() {
        if ( $(this).val() === '' )
          isValid = false;
      });
      if(isValid) {
        $(form + ' .btn').prop('disabled', false);
      }
    }


    // animation
    $('.animated').appear(function() {
        var elem = $(this);
        var animation = elem.data('animation');

        if ( !elem.hasClass('visible') ) {
            var animationDelay = elem.data('animation-delay');
            if ( animationDelay ) {
                setTimeout(function(){
                    elem.addClass( animation + " visible" );
                }, animationDelay);

            } else {
                elem.addClass( animation + " visible" );
            }
        }
    });
    // animation

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    $(".slider-main__item").hover(
      function() {
        var gold = $(this).data('gold');
        var silver = $(this).data('silver');
        $('.slider-main__item-txt').html('<div class="text--warning text__sm"><div class="text__md">'+gold+'%</div>Gold</div>' +
            '<div class="text--white text__sm"><div class="text__md">'+silver+'%</div>Silver</div>');
      }
    );

    // slider
    $('.slider').slick({
        slidesToShow: 10,
        nextArrow: '<div class="slider-arrow-left slick-arrow"><i class="icon-next"></i></div>',
        prevArrow: '<div class="slider-arrow-right slick-arrow"><i class="icon-prev"></i></div>',
        arrows: true,
        infinite: false,
        draggable: false,
        responsive: [
         {
            breakpoint: 1400,
            settings: {
              slidesToShow: 8
            }
        },
         {
            breakpoint: 940,
            settings: {
              slidesToShow: 11
            }
         },
          {
             breakpoint: 700,
             settings: {
               // slidesToShow: 5,
               variableWidth: true,
               arrows: false
             }
          }
       ]
    });
    $('.slider-shop').slick({
        slidesToShow: 6,
        slidesToScroll: 2,
        nextArrow: '<div class="slider-arrow-left slick-arrow slick-arrow--s"><i class="icon-right-open-big"></i></div>',
        prevArrow: '<div class="slider-arrow-right slick-arrow slick-arrow--s"><i class="icon-left-open-big"></i></div>',
        arrows: true,
        responsive: [
         {
            breakpoint: 1400,
            settings: {
              slidesToShow: 4
            }
        },
         {
            breakpoint: 700,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              dots: true
            }
         }
       ]
    });
    $('.reviews-block').slick({
        adaptiveHeight: true,
        slidesToShow: 4,
        arrows: true,
        nextArrow: '<div class="slider-arrow-left slick-arrow slick-arrow--s"><i class="icon-right-open-big"></i></div>',
        prevArrow: '<div class="slider-arrow-right slick-arrow slick-arrow--s"><i class="icon-left-open-big"></i></div>',
        responsive: [
             {
                breakpoint: 940,
                settings: {
                  slidesToShow: 1
                }
            },
           {
               breakpoint: 700,
               settings: {
                 asNavFor: '.slider-nav',
                 slidesToShow: 1,
                 arrows: false
               }
           }
       ]
    });
    $('.slider-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        asNavFor: '.reviews-block',
        arrows: false,
        centerMode: true,
        focusOnSelect: true
    });
    $('.about-block').slick({
        slidesToShow: 5,
        arrows: true,
        nextArrow: '<div class="slider-arrow-left slick-arrow slick-arrow--s"><i class="icon-right-open-big"></i></div>',
        prevArrow: '<div class="slider-arrow-right slick-arrow slick-arrow--s"><i class="icon-left-open-big"></i></div>',
        responsive: [
         {
            breakpoint: 1400,
            settings: {
              slidesToShow: 4
            }
        },
         {
            breakpoint: 940,
            settings: {
              slidesToShow: 3
            }
         },
          {
             breakpoint: 700,
             settings: {
               slidesToShow: 2,
               dots: true,
               arrows: false
             }
          }
        ]
    });
    // slider

    jQuery.fn.extend({
        toggleText: function (a, b){
            var that = this;
                if (that.text() != a && that.text() != b){
                    that.text(a);
                }
                else
                if (that.text() == a){
                    that.text(b);
                }
                else
                if (that.text() == b){
                    that.text(a);
                }
            return this;
        }
    });

    //.filter--js
    $('.filter--js').click(function () {
       $(this).toggleClass('active');
       $('.filter-txt--js').toggleText('Обычный поиск', 'Расширеный поиск');
       $('.filter-body--js').toggleClass('active');
       return false;
    });
    //.filter--js

    //.userlocation-close--js
    $('.userlocation-close--js').click(function () {
       $(this).parent().slideUp();
    });
    //.userlocation-close--js

    // select {select2}
    $("select.control").select2({
        minimumResultsForSearch: Infinity,
        width: 'resolve'
    });
    // select

    // popup {magnific-popup}
    $('.popup').magnificPopup({
  		type: 'inline',

  		fixedContentPos: false,
  		fixedBgPos: true,

  		overflowY: 'auto',
  		removalDelay: 300,
      closeMarkup: '<a class="popup-close popup-modal-dismiss" href="#"><i class="icon-close"></i></a>',
  		mainClass: 'my-mfp-zoom-in'
  	});
  	$(document).on('click', '.popup-close', function (e) {
  		e.preventDefault();
  		$.magnificPopup.close();
  	});
    // popup

    $('.accordion__title').click(function() {
        $(this).parent().siblings('.accordion__item').find('.accordion__body').slideUp('fast');
        $(this).next('.accordion__body').slideToggle('fast');
    });

    $(".slider-main__item").click(
      function() {
        $('.slider-main__item-txt').html('');
        var gold = $(this).data('gold');
        var silver = $(this).data('silver');
        $('.slider-main__item-txt').html('<div class="text--warning text__sm"><div class="text__md">'+gold+'%</div>Gold</div><div class="text--white text__sm"><div class="text__md">'+silver+'%</div>Silver</div>');
      }
    );

    //copy to buffer
    var clipboardDemos = new Clipboard('[data-clipboard-demo]');
    //copy to buffer


    $(".filter-dropdown-input").on('click', function () {
        $(this).next().slideToggle('fast');
    });
    $(document).bind('click', function (e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("filter-dropdown")) {
            $(".filter-dropdown-body").hide();
        }
    });


    $('.filterme--js').on('input',function(e){
        jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
            return function( elem ) {
                return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });
        $(this).next().find('li').css('display', 'none');
        var filterVal = $(this).val();
        $(this).next().find('label').parent().parent()
        $(this).next().find('label:Contains("'+filterVal+'")').parent().parent()
        .css('display', 'block');
    });

    $('.filter-dropdown-body input[type="checkbox"]').on('click', function () {
          var title = $(this).closest('.filter-dropdown-body').find('input[type="checkbox"]').val(),title = $(this).val();
          if ($(this).is(':checked')) {
              var html = '<span title="' + title + '">' + title + '</span>';
              $(this).closest('.filter-dropdown').find('.multiSel').append(html);
              var length = $(this).closest('.filter-dropdown').find('.multiSel span').length;
              $(this).closest('.filter-dropdown').find(".multiCount span").html(length);
              $(this).closest('.filter-dropdown').addClass('active');
          }
          else {
              $('span[title="' + title + '"]').remove();
              var length = $(this).closest('.filter-dropdown').find('.multiSel span').length;
              $(this).closest('.filter-dropdown').find(".multiCount span").html(length);
              if(length < '1') {
                  $(this).closest('.filter-dropdown').find('.multiCount span').text('');
                  $(this).closest('.filter-dropdown').removeClass('active');
              }
          }
      });
});
// ready

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {

}
// mobile sctipts
